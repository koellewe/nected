# Nected Android Client

Notice: Due to Facebook's policy updates ([April 2018](https://www.facebook.com/about/privacy/update)), Nected can no longer function and so the project has been abandoned.

Details on how Nected works is available at the [backend repo](https://bitbucket.org/jansgeorge/nected-backend).