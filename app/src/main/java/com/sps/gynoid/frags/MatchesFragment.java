package com.sps.gynoid.frags;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.sps.gynoid.MatchesActivity;
import com.sps.gynoid.R;
import com.sps.gynoid.utils.ExpandedListView;
import com.sps.gynoid.utils.JanTools;
import com.sps.gynoid.utils.Match;
import com.sps.gynoid.utils.PrefsHelper;
import com.sps.gynoid.utils.RautenAPI;
import com.sps.gynoid.utils.RemovalDialog;
import com.sps.gynoid.utils.Rjson;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import de.hdodenhof.circleimageview.CircleImageView;

public class MatchesFragment extends Fragment {

    public static final String DIR_LOCAL_PP = "downloaded_pps";
    private static String path_app_files;
    public MatchesFragment() {
        // Required empty public constructor
    }

    private ExpandedListView listMatches;
    private ExpandedListView listFakeMatches;

    private Match[] matches = null;
    private List<GetPP> ppGetters;
    private MatchFragCallback callback = null;
    private PrefsHelper prefs;

    public void setMatches(Match[] matches){
        this.matches = matches;
        ppGetters = new ArrayList<>();
    }

    public void setCallback(MatchFragCallback callback){
        this.callback = callback;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        prefs = new PrefsHelper(inflater.getContext());
        path_app_files = inflater.getContext().getFilesDir().getAbsolutePath();

        View view = inflater.inflate(R.layout.fragment_matches, container, false);

        listMatches = view.findViewById(R.id.listMatches);
        TextView txtBetweenLists = view.findViewById(R.id.txtBetweenLists);
        listFakeMatches = view.findViewById(R.id.listFakeMatches);

        if (matches != null){ //bullshit call

            int legitMatches = 0;
            int illegitMatches = 0;
            for (Match match : matches){
                if (match.legit)
                    legitMatches++;
                else
                    illegitMatches++;
            }

            if (legitMatches > 0 && illegitMatches == 0){

                listMatches.setAdapter(
                        new MatchListAdapter(inflater, matches, true)
                );

                txtBetweenLists.setVisibility(View.GONE);
                listFakeMatches.setVisibility(View.GONE);

            }else if (legitMatches == 0 && illegitMatches > 0){

                listMatches.setVisibility(View.GONE);

                txtBetweenLists.setText(R.string.only_illegit_matches_between_text);

                listFakeMatches.setAdapter(
                        new MatchListAdapter(inflater, matches, false)
                );

            }else{ //both legit & illegit

                listMatches.setAdapter(
                        new MatchListAdapter(inflater, onlyLegitMatches(true), true)
                );

                txtBetweenLists.setText(R.string.legit_and_illegit_matches_between_text);

                listFakeMatches.setAdapter(
                        new MatchListAdapter(inflater, onlyLegitMatches(false), false)
                );

            }

            ((SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshMatches))
                    .setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                        @Override
                        public void onRefresh() {
                            callback.onSwipeRefresh();
                        }
                    });

        }

        return view;

    }

    @Override
    public void onResume() {
        super.onResume();
        new WaitToGetPPs().execute();
    }

    private class MatchListAdapter extends ArrayAdapter<Match>{

        private LayoutInflater inflater;
        private Match[] spec_matches;
        private boolean legit;

        MatchListAdapter (LayoutInflater inflater, Match[] spec_matches, boolean legit){
            super(inflater.getContext(), R.layout.item_match, spec_matches);
            this.inflater = inflater;
            this.spec_matches = spec_matches;
            this.legit = legit;
        }

        @NonNull
        @Override
        public View getView(final int position, @Nullable View convertView, @NonNull final ViewGroup parent) {

            if (convertView == null){
                convertView = inflater.inflate(R.layout.item_match, parent, false);
            }


            GetPP ppGetter = new GetPP(
                    spec_matches[position].profileID,
                    ((CircleImageView) convertView.findViewById(R.id.imgPP)),
                    ((ProgressBar) convertView.findViewById(R.id.progPP)),
                    position, legit //these two vars are for unique identification
            );

            if (!isPPRegistered(position, legit)){
                ppGetters.add(ppGetter);
            }else if (ppGetters.get(position).getStatus() == AsyncTask.Status.PENDING){
                removePPGetter(position, legit);
                ppGetters.add(ppGetter);
            }

            ((TextView) convertView.findViewById(R.id.txtName)).setText(
                    String.format(Locale.getDefault(), "%s %s", spec_matches[position].firstName,
                            spec_matches[position].lastName.substring(0, 1).toUpperCase()
                    )
            );

            ((TextView) convertView.findViewById(R.id.txtSub)).setText(
                    String.format(getString(R.string.match_quality_format), spec_matches[position].matchQuality)
            );

            final JanTools janTools = new JanTools(inflater.getContext());
            if (janTools.isPackageInstalled("com.facebook.orca")) {
                ((ImageButton) convertView.findViewById(R.id.btnMessage)).setImageDrawable(
                        janTools.scaleDrawable(janTools.getAppIcon("com.facebook.orca"), 75)
                );
            }

            final ProgressBar progIgnore = convertView.findViewById(R.id.progIgnore);

            convertView.findViewById(R.id.btnIgnore).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    RemovalDialog.newInstance(inflater.getContext(),
                            spec_matches[position].firstName,
                            new RemovalDialog.Callback() {
                                @Override
                                public void rate(final int rating) {

                                    progIgnore.setVisibility(View.VISIBLE);

                                    new RautenAPI(RautenAPI.URL_USER_INTERACTION,
                                            new Rjson().put("action", "rate|ignore")
                                                    .put("rauth", prefs.readStr(PrefsHelper.PREF_RAUTH))
                                                    .put("ratee", spec_matches[position].profileID)
                                                    .put("rating", rating),
                                            new RautenAPI.Callback() {

                                                @Override
                                                public void onSuccess(@NonNull JSONObject result) {
                                                    Log.i("devlog", "User rated " + rating);
                                                    progIgnore.setVisibility(View.GONE);

                                                    removeMatchFromUI(spec_matches[position].profileID, inflater.getContext());
                                                }

                                                @Override
                                                public void onFail(@Nullable JSONObject result) {
                                                    progIgnore.setVisibility(View.GONE);
                                                    janTools.makeToast(R.string.rating_failed_toast);
                                                }

                                            }).executeAsync();

                                }

                                @Override
                                public void report(final String[] reasons) {

                                    progIgnore.setVisibility(View.VISIBLE);

                                    JSONArray jReasons = new JSONArray();
                                    for (String reason : reasons) {
                                        jReasons.put(reason);
                                    }

                                    new RautenAPI(RautenAPI.URL_USER_INTERACTION,
                                            new Rjson().put("action", "report|ignore")
                                                    .put("rauth", prefs.readStr(PrefsHelper.PREF_RAUTH))
                                                    .put("reportee", spec_matches[position].profileID)
                                                    .put("reasons", jReasons),
                                            new RautenAPI.Callback() {

                                                @Override
                                                public void onSuccess(@NonNull JSONObject result) {

                                                    Log.i("devlog", "User reported for: " + Arrays.toString(reasons));
                                                    progIgnore.setVisibility(View.GONE);

                                                    removeMatchFromUI(spec_matches[position].profileID, inflater.getContext());

                                                }

                                                @Override
                                                public void onFail(@Nullable JSONObject result) {
                                                    progIgnore.setVisibility(View.GONE);
                                                    janTools.makeToast(R.string.report_user_failed_toast);
                                                }

                                            }).executeAsync();

                                }
                            }
                    ).show(getChildFragmentManager(), RemovalDialog.TAG);

                }
            });

            final ProgressBar progMsg = convertView.findViewById(R.id.progMsg);

            convertView.findViewById(R.id.btnMessage).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    final PrefsHelper prefs = new PrefsHelper(parent.getContext());

                    if (!prefs.readBool(PrefsHelper.PREF_DIA_REPORT_MSGER)) {

                        new AlertDialog.Builder(parent.getContext())
                                .setMessage(String.format(getString(R.string.dialog_report_bad_msgr_profile),
                                        spec_matches[position].firstName))
                                .setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        prefs.write(PrefsHelper.PREF_DIA_REPORT_MSGER, true);
                                        goToMessenger();
                                    }
                                })
                                .show();
                    } else {
                        goToMessenger();
                    }

                }

                private void goToMessenger() {
                    new HandleMsgProg(progMsg).execute();
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://m.me/" + spec_matches[position].messengerUsername)));
                }
            });

            if (position == 0) {
                convertView.setPadding(convertView.getPaddingLeft(), ((int) janTools.convertDpToPixels(16)), convertView.getPaddingRight(), convertView.getPaddingBottom());
            } else if (position == getCount() - 1) {
                convertView.setPadding(convertView.getPaddingLeft(), convertView.getPaddingTop(), convertView.getPaddingRight(), ((int) janTools.convertDpToPixels(16)));
            }

            return convertView;

        }

        private boolean isPPRegistered(int posInSpecMatches, boolean specMatchesLegit){

            for(int i = 0; i < ppGetters.size(); i++){
                if (ppGetters.get(i).correspondingMatchPos == posInSpecMatches && ppGetters.get(i).forLegit == specMatchesLegit)
                    return true;
            }

            return false;

        }

        private void removePPGetter(int posInSpecMatches, boolean specMatchesLegit){

            for (int i = 0; i < ppGetters.size(); i++){
                if (ppGetters.get(i).correspondingMatchPos == posInSpecMatches && ppGetters.get(i).forLegit == specMatchesLegit)
                    ppGetters.remove(i);
            }

        }
    }

    private void getAllPPs(){
        for (GetPP ppGetter : ppGetters){
            if (ppGetter.getStatus() == AsyncTask.Status.PENDING) {
                ppGetter.execute();
            }
        }
    }

    private void removeMatchFromUI(String profileID, Context context){

        //copy matches (except ratee) to updatedMatches
        Match[] updatedMatches = new Match[matches.length-1];
        int j = 0;
        for (Match match : matches)
            if (!match.profileID.equals(profileID)) {
                updatedMatches[j] = match;
                j++;
            }

        LocalBroadcastManager.getInstance(context)
                .sendBroadcast(new Intent(MatchesActivity.FILTER_LOCAL)
                        .putExtra("event", MatchesActivity.EVENT_MATCHES_BEGOTTEN)
                        .putExtra("matches", updatedMatches)
                );

    }

    //if true, it returns the legit matches subarray
    //else it returns the illegit matches subarray
    private Match[] onlyLegitMatches(boolean legitimacy){

        int legit_matches = 0;
        for (Match match : matches){
            if (match.legit == legitimacy)
                legit_matches++;
        }

        Match[] specific_matches = new Match[legit_matches];
        int j = 0;
        for (Match match : matches) {
            if (match.legit == legitimacy) {
                specific_matches[j] = match;
                j++;
            }
        }

        return specific_matches;

    }

    private class HandleMsgProg extends AsyncTask<Void, Void, Void>{

        private WeakReference<ProgressBar> prog;

        HandleMsgProg(ProgressBar prog){
            this.prog = new WeakReference<>(prog);
        }

        @Override
        protected void onPreExecute() {
            if (prog.get() != null)
                prog.get().setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try{
                Thread.sleep(1000);
            }catch (InterruptedException ignored){}

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (prog.get() != null)
                prog.get().setVisibility(View.GONE);
        }
    }

    private class WaitToGetPPs extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {

            try{
                Thread.sleep(200);
            }catch (InterruptedException ignored){}

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (isVisible()) {
                getAllPPs();
            }else
                new WaitToGetPPs().execute();
        }
    }

    private class GetPP extends AsyncTask<Void, Void, Bitmap>{

        int correspondingMatchPos = -1;
        boolean forLegit = true;

        private String profileID;
        private WeakReference<CircleImageView> imgHandle;
        private WeakReference<ProgressBar> progHandle;

        GetPP(String profileID, CircleImageView imgView, ProgressBar progOverImg, int correspondingMatchPos, boolean forLegit){
            this.profileID = profileID;
            imgHandle = new WeakReference<>(imgView);
            progHandle = new WeakReference<>(progOverImg);
            this.correspondingMatchPos = correspondingMatchPos;
            this.forLegit = forLegit;
        }

        @Override
        protected Bitmap doInBackground(Void... v) {

            FileOutputStream out = null;
            try {

                if (imgHandle.get() != null) {

                    File localPP = new File(path_app_files + "/" +
                            DIR_LOCAL_PP + "/" + profileID + ".jpg");

                    if (localPP.exists()){
                        return BitmapFactory.decodeFile(localPP.getAbsolutePath());

                    }else {

                        String raw = Jsoup.connect("https://graph.facebook.com/v2.12/" + profileID + "/picture?type=normal&redirect=0")
                                .ignoreContentType(true).get().select("body").html();
                        String rurl = new JSONObject(raw).getJSONObject("data").getString("url");
                        URL ppUrl = new URL(rurl.replace("&amp;", "&")); //no idea why this is an issue, but luckily easy to solve

                        HttpURLConnection connection = ((HttpURLConnection) ppUrl.openConnection());
                        connection.connect();

                        Bitmap retrievedPP = BitmapFactory.decodeStream(connection.getInputStream());

                        localPP.getParentFile().mkdir(); localPP.createNewFile();
                        out = new FileOutputStream(localPP);
                        retrievedPP.compress(Bitmap.CompressFormat.JPEG, 100, out);
                        out.flush(); out.close();

                        return retrievedPP;
                    }

                }else{
                    return null;
                }

            }catch (IOException|JSONException e){
                e.printStackTrace();
                Log.w("devlog", "Failed to get a pp");
                return null;

            }

        }

        @Override
        protected void onPostExecute(Bitmap pp) {

            if (pp != null) {

                if (imgHandle.get() != null) {
                    imgHandle.get().setImageBitmap(pp);
                }

            }

            if (progHandle.get() != null){
                progHandle.get().setVisibility(View.GONE);
            }

        }
    }

    public interface MatchFragCallback{
        void onSwipeRefresh();
    }

}
