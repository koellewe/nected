package com.sps.gynoid.frags;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.Profile;
import com.sps.gynoid.R;

public class StartInfoFrag extends Fragment {

    private View.OnClickListener onDoneClickListener;

    public static StartInfoFrag newInstance(View.OnClickListener onDoneClickListener){
        StartInfoFrag frag = new StartInfoFrag();
        frag.setOnDoneClickListener(onDoneClickListener);
        return frag;
    }

    private void setOnDoneClickListener(View.OnClickListener onDoneClickListener){
        this.onDoneClickListener = onDoneClickListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_start_info, container, false);

        ((TextView) view.findViewById(R.id.txtWelcome)).setText(String.format(getString(R.string.welcome_user_title), Profile.getCurrentProfile().getFirstName()));

        ((TextView) view.findViewById(R.id.txtInfoTitle1)).setText(String.format(getString(R.string.matches_on_app_special_title), getString(R.string.app_name)));

        view.findViewById(R.id.btnInfoDone).setOnClickListener(onDoneClickListener);

        return view;
    }

}
