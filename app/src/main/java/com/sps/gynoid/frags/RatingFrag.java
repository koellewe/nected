package com.sps.gynoid.frags;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RatingBar;
import android.widget.TextView;

import com.sps.gynoid.R;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

/**
 * Created by koell on 29 Mar 2018.
 */

public class RatingFrag extends Fragment {

    private int ratingPerc;

    private Callback callback;
    private String name;

    private ViewGroup viewGroup;

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {

        viewGroup = ((ViewGroup) inflater.inflate(R.layout.dialog_rating, container, false));

        ((TextView) viewGroup.findViewById(R.id.txtHowWasIt)).setText(String.format(getString(R.string.how_was_it_dia), name));

        ((MaterialRatingBar) viewGroup.findViewById(R.id.mrbExperience))
                .setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

                if (fromUser) { //weird glitch
                    ratingBar.setRating(((float) Math.ceil(rating)));
                }

                ratingPerc = (int) Math.round((Math.ceil(rating)/ratingBar.getNumStars()) * 100);

                if (ratingPerc > 0){
                    viewGroup.findViewById(R.id.btnRatingDone).setEnabled(true);
                }

                if (ratingPerc <= 20){
                    viewGroup.findViewById(R.id.layReport).setVisibility(View.VISIBLE);

                }else{
                    viewGroup.findViewById(R.id.layReport).setVisibility(View.GONE);
                    ((CheckBox) viewGroup.findViewById(R.id.chkReport)).setChecked(false);
                    updateDoneButton(((CheckBox) viewGroup.findViewById(R.id.chkReport)));
                }

            }
        });

        ((CheckBox) viewGroup.findViewById(R.id.chkReport)).setText(String.format(getString(R.string.report_user_chk), name));
        viewGroup.findViewById(R.id.chkReport).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateDoneButton(((CheckBox) v));
            }
        });

        viewGroup.findViewById(R.id.btnRatingDone).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.ratingDone(ratingPerc,
                        ((CheckBox) viewGroup.findViewById(R.id.chkReport)).isChecked()
                        && (ratingPerc <= 20)
                );
            }
        });

        return viewGroup;

    }

    private void updateDoneButton(CheckBox chk){

        if (chk.isChecked()){
            ((Button) viewGroup.findViewById(R.id.btnRatingDone)).setText(R.string.settings_setup_next);
        }else{
            ((Button) viewGroup.findViewById(R.id.btnRatingDone)).setText(R.string.rating_submit_dia);
        }

    }

    private void setCallback(Callback callback) {
        this.callback = callback;
    }
    private void setName(String name) {
        this.name = name;
    }

    public static RatingFrag newInstance(Callback callback, String name){

        RatingFrag frag = new RatingFrag();
        frag.setCallback(callback);
        frag.setName(name);

        return frag;

    }

    public interface Callback{
        void ratingDone(int rating, boolean reportUser);
    }

}
