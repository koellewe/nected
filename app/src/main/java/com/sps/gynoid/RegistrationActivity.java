package com.sps.gynoid;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.sps.gynoid.frags.ContactFrag;
import com.sps.gynoid.frags.LoginFrag;
import com.sps.gynoid.frags.SetsSetupFrag;
import com.sps.gynoid.utils.Dialogger;
import com.sps.gynoid.utils.JanTools;
import com.sps.gynoid.utils.NonSwipeableViewPager;
import com.sps.gynoid.utils.PrefsHelper;
import com.sps.gynoid.utils.RautenAPI;
import com.sps.gynoid.utils.Rjson;

import org.json.JSONException;
import org.json.JSONObject;

public class RegistrationActivity extends FragmentActivity {

    private NonSwipeableViewPager viewPager;
    private JanTools jantools;
    private PrefsHelper prefs;
    private LoginFrag loginFrag = null;
    private SetsSetupFrag setsSetupFrag = null;
    private ContactFrag contactFrag = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        jantools = new JanTools(this);
        prefs = new PrefsHelper(this);

        viewPager = findViewById(R.id.viewPager);
        viewPager.setAdapter(
                new RegPagerAdapter(getSupportFragmentManager())
        );
    }

    private class RegPagerAdapter extends FragmentPagerAdapter{

        RegPagerAdapter(FragmentManager fm){
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            switch(position){

                case 0:
                    if (loginFrag == null) {
                        loginFrag = LoginFrag.newInstance(new LoginFrag.OnLoginDoneListener() {
                            @Override
                            public void onLoginDone(boolean alreadyRegistered) {
                                if (alreadyRegistered){
                                    prefs.write(PrefsHelper.PREF_REG_DONE, true);
                                    startActivity(
                                            new Intent(RegistrationActivity.this, MatchesActivity.class)
                                    );
                                    finish();
                                }else {
                                    viewPager.setCurrentItem(1, true);
                                }
                            }
                        });
                    }
                    return loginFrag;

                case 1:
                    if (setsSetupFrag == null) {
                        setsSetupFrag = SetsSetupFrag.newInstance(new SetsSetupFrag.OnSetsSetUpListener() {
                            @Override
                            public void setsSetUp() {
                                viewPager.setCurrentItem(2, true);
                            }

                            @Override
                            public void setsFail() {
                                LoginManager.getInstance().logOut();
                                viewPager.setCurrentItem(0, true);
                            }
                        });
                    }
                    return setsSetupFrag;


                case 2:
                    if (contactFrag == null){
                        contactFrag = ContactFrag.newInstance(new ContactFrag.ContactDoneListener() {
                            @Override
                            public void onContactDetailsVerified() {

                                //set user to registered

                                new RautenAPI(RautenAPI.URL_USER_HANDLER, new Rjson()
                                        .put("action", "finish_registration")
                                        .put("profileID", Profile.getCurrentProfile().getId()),
                                        new RautenAPI.Callback() {

                                            @Override
                                            public void onSuccess(@NonNull JSONObject result) throws JSONException {

                                                prefs.write(PrefsHelper.PREF_RAUTH, result.getString("rauth"));
                                                prefs.write(PrefsHelper.PREF_REG_DONE, true);
                                                startActivity(
                                                        new Intent(RegistrationActivity.this, MatchesActivity.class)
                                                );
                                                RegistrationActivity.this.finish();

                                            }

                                            @Override
                                            public void onFail(@Nullable JSONObject result) {
                                                Log.w("devlog", "Could not mark registration done");
                                                jantools.makeToast(R.string.registration_fail_toast);
                                            }
                                            
                                        },
                                        new Dialogger(RegistrationActivity.this)
                                            .setMessage(R.string.finishing_reg_dia)
                                            .build()
                                ).executeAsync();

                            }
                        });
                    }
                    return contactFrag;


                default:
                    Log.w("devlog", "unknown viewpager position: "+position);
                    return null;
            }

        }

        @Override
        public int getCount() {
            return 3;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (loginFrag != null)
            if (!loginFrag.isHidden())
                loginFrag.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (loginFrag != null)
            if (!loginFrag.isHidden())
                loginFrag.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
