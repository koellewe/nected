package com.sps.gynoid.utils;

import android.app.AlertDialog;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by koell on 17 Oct 2017.
 */

public class RautenAPI {

    private static final boolean VERBOSE_MODE = false;
    private static final String BACKEND_VERSION = "v2";

    public static final class Setting {
        public static final String MATCH_QUALITY = "MATCH_QUALITY";
        public static final String MAX_DISTANCE_INDEX = "MAX_DISTANCE_INDEX";
        public static final String MIN_AGE = "MIN_AGE";
        public static final String MAX_AGE = "MAX_AGE";
        public static final String LIKES_MEN = "LIKES_MEN";
        public static final String LIKES_WOMEN = "LIKES_WOMEN";
    }

    public static final class SettingAction {
        public static final String SAVE = "save";
        public static final String GET = "get";
    }

    private static final String BACKEND_PATH = "https://apps.rauten.co.za/gynoid/"+BACKEND_VERSION;
    public static final String URL_USER_HANDLER = BACKEND_PATH +"/user_handler.php";
    public static final String URL_MATCH_HANDLER = BACKEND_PATH +"/match_handler.php";
    public static final String URL_SETTINGS_HANDLER = BACKEND_PATH +"/settings_handler.php";
    public static final String URL_LOCATION_HANDLER = BACKEND_PATH +"/location_handler.php";
    public static final String URL_USER_INTERACTION = BACKEND_PATH +"/user_interaction.php";

    private String connUrl;
    private Rjson jsonToSend;
    private Callback callback;
    private AlertDialog progDialog;

    public RautenAPI(String connUrl){
        this(connUrl, null, null);
    }

    public RautenAPI(String connURL, Rjson jsonToSend, Callback callback){
        this(connURL, jsonToSend, callback, null);
    }

    public RautenAPI(String connURL, Rjson jsonToSend, Callback callback, AlertDialog progDialog){
        this.connUrl = connURL;
        this.jsonToSend = jsonToSend;
        this.callback = callback;
        this.progDialog = progDialog;
    }

    public void executeAsync(){
        new AsyncExecutive().execute();
    }

    private class AsyncExecutive extends AsyncTask<Void, Void, JSONObject>{

        @Override
        protected void onPreExecute() {
            if (progDialog != null)
                progDialog.show();
        }

        @Override
        protected JSONObject doInBackground(Void... v) {

            try{
                return makeReq(jsonToSend);
            }catch (JSONException| IOException e){
                if (VERBOSE_MODE) {
                    e.printStackTrace();
                }else{
                    Log.w("devlog", "RautenAPI failure: " + e.getMessage());
                }
                return null;
            }

        }

        @Override
        protected void onPostExecute(JSONObject result) {
            if (progDialog != null)
                progDialog.dismiss();

            try {
                if (result == null) {
                    callback.onFail(null);
                } else if (result.getBoolean("success")) {
                    try {
                        callback.onSuccess(result);
                    }catch (JSONException e){
                        Log.w("devlog","JSONException in onSuccess: " + e.getMessage());
                        callback.onFail(null);
                    }
                }else{
                    callback.onFail(result);
                }
            }catch (JSONException e){
                callback.onFail(null);
            }
        }
    }

    public @NonNull JSONObject makeReq(Rjson jsonToSend) throws JSONException, IOException{

        if (VERBOSE_MODE){
            Log.d("devlog", "RautenAPI----------");
            Log.d("devlog", "sending to: " + connUrl);
        }

        HttpURLConnection http = ((HttpURLConnection) new URL(connUrl).openConnection());
        http.setRequestMethod("POST");
        http.setConnectTimeout(3_000);
        http.setReadTimeout(8_000);
        http.setRequestProperty("Content-Type", "application/json");
        http.setRequestProperty("Accept", "application/json");
        http.setDoOutput(true);
        http.setDoInput(true);

        OutputStreamWriter sender = new OutputStreamWriter(http.getOutputStream());
        sender.write(jsonToSend.toString());
        sender.flush();
        sender.close();

        if (VERBOSE_MODE) {
            Log.d("devlog", "SENT: " + jsonToSend.toString());
        }

        int responseCode = http.getResponseCode();
        if (responseCode != HttpURLConnection.HTTP_OK) {
            throw new IOException("Server responded with non-OK HTTP code");
        }else{

            StringBuilder responseRaw = new StringBuilder();
            BufferedReader receiver = new BufferedReader(new InputStreamReader(http.getInputStream()));
            String line;
            while ((line = receiver.readLine()) != null){
                responseRaw.append(line);
            }
            receiver.close();

            if (VERBOSE_MODE) {
                Log.d("devlog", "RECEIVED: " + responseRaw.toString());
            }

            return new JSONObject(responseRaw.toString());

        }


    }

    public interface Callback{
        void onSuccess(@NonNull JSONObject result) throws JSONException;
        void onFail(@Nullable JSONObject result);
    }

}
