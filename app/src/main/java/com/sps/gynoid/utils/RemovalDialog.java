package com.sps.gynoid.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.sps.gynoid.R;
import com.sps.gynoid.frags.RatingFrag;
import com.sps.gynoid.frags.ReportFrag;

/**
 * Created by koell on 14 Apr 2018.
 */

public class RemovalDialog extends DialogFragment {

    public static final String TAG = "fragment_dialog";

    private Context ctx;
    private String name;
    private Callback callback;

    private NonSwipeableViewPager viewPager;
    private RatingFrag ratingFrag = null;
    private ReportFrag reportFrag = null;

    public static RemovalDialog newInstance(Context context, String name, Callback callback){
        RemovalDialog dialog = new RemovalDialog();
        dialog.ctx = context;
        dialog.name = name;
        dialog.callback = callback;

        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = ((LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                .inflate(R.layout.dialog_rate_report, null);

        viewPager = v.findViewById(R.id.viewPager);
        viewPager.setAdapter(new RemovalDialogPagerAdapter(
                getChildFragmentManager()
        ));

        return v;

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    private class RemovalDialogPagerAdapter extends FragmentPagerAdapter {

        RemovalDialogPagerAdapter(FragmentManager fm){super(fm);}

        @NonNull
        @Override
        public Fragment getItem(int position) {

            switch (position){

                case 0:
                    if (ratingFrag == null)
                        ratingFrag = RatingFrag.newInstance(
                                new RatingFrag.Callback() {
                                    @Override
                                    public void ratingDone(int rating, boolean reportUser) {
                                        if (reportUser) {
                                            viewPager.setCurrentItem(1, true);
                                        } else {
                                            dismiss();
                                            callback.rate(rating);
                                        }
                                    }
                                },
                                name
                        );
                    return ratingFrag;

                case 1:
                    if (reportFrag == null){
                        reportFrag = ReportFrag.newInstance(new ReportFrag.Callback() {
                            @Override
                            public void reportDone(String[] reasons) {
                                dismiss();
                                callback.report(reasons);
                            }
                        });
                    }
                    return reportFrag;

                default:
                    Log.w("devlog", "unkown removal dialog viewpager index: " + position);
                    return null; //it's supposed to crash

            }

        }

        @Override
        public int getCount() {
            return 2;
        }
    }

    public interface Callback{
        void rate(int rating);
        void report(String[] reasons);
    }

}
