package com.sps.gynoid.utils;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by koell on 29 Jan 2018.
 */

public class Match implements Parcelable{

    public String profileID;
    public String firstName;
    public String lastName;
    public int age;
    public double matchQuality;
    public String messengerUsername;
    public boolean legit;

    public Match(String profileID, String firstName, String lastName, int age, double matchQuality, String messengerUsername, boolean legit){
        this.profileID = profileID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.matchQuality = matchQuality;
        this.messengerUsername = messengerUsername;
        this.legit = legit;
    }
    private Match(Parcel in){
        this.profileID = in.readString();
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.age = in.readInt();
        this.matchQuality = in.readDouble();
        this.messengerUsername = in.readString();
        this.legit = in.readInt() == 1;
    }

    public @NonNull JSONObject toJSON(){

        try {
            return new JSONObject()
                    .put("profileID", profileID)
                    .put("first_name", firstName)
                    .put("last_name", lastName)
                    .put("age", age)
                    .put("quality", matchQuality)
                    .put("messengerUsername", messengerUsername)
                    .put("legitimacy", legit ? 1 : 0);

        }catch (JSONException e){
            Log.w("devlog", "could not convert a match param to json. returning empty obj");
            return new JSONObject();
        }

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeString(profileID);
        parcel.writeString(firstName);
        parcel.writeString(lastName);
        parcel.writeInt(age);
        parcel.writeDouble(matchQuality);
        parcel.writeString(messengerUsername);
        parcel.writeInt(legit ? 1 : 0);

    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Match createFromParcel(Parcel in) {
            return new Match(in);
        }

        public Match[] newArray(int size) {
            return new Match[size];
        }
    };

}
